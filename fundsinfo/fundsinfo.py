#!/home/psysc0rpi0n/Downloads/dev/plugins/bin/python

from pyln.client import Plugin

plugin = Plugin()


@plugin.init()
def init(options, plugin, configuration, **kwargs):
    plugin.log("Plugin fundscollector loaded!")

# Notes
# "fundscollector" in <@plugin.method("fundscollector")> defines the plugin
# name
# when we want to run it already inside CLN

# "out_opt" in <out_opt = plugin.get_option("out_opt")> and in
# <plugin.add_option('out_opt', 'test_default', 'Type of funds to show')>
# defines one option to pass to the plugin when LOADING the plugin


def funds_type_data(fundstype='onchain'):
    data = plugin.rpc.listfunds()
    if data:
        if fundstype == 'onchain':
            json_data = plugin.rpc.listfunds().get('outputs')
        else:
            json_data = plugin.rpc.listfunds().get('channels')
    else:
        return None
    return json_data


def get_funds(json_data):
    funds = 0
    for idx, data_objs in enumerate(json_data):
        for key, value in data_objs.items():
            if key == "amount_msat" or key == "our_amount_msat":
                funds += int(value)
    return funds


@plugin.method("fundsinfo")
def print_funds_info(plugin, fundstype='all'):
    onchain_funds = get_funds(funds_type_data('onchain'))
    offchain_funds = get_funds(funds_type_data('offchain'))
    misc_funds = onchain_funds + offchain_funds
    match fundstype:
        case 'onchain':
            return {
                    "Onchain funds": onchain_funds,
                   }
        case 'offchain':
            return {
                    "Offchain funds": offchain_funds,
                   }
        case 'misc':
            return {
                    "Misc funds": misc_funds,
                   }
        case 'all':
            return {
                    "Onchain funds": onchain_funds,
                    "Offchain funds": offchain_funds,
                    "Misc funds": misc_funds,
                   }
        case _:
            return {
                    "No funds or unknown error!",
                   }


plugin.run()
