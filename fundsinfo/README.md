# Plugin name: fundsinfo 

## Plugin description
    Fundsinfo is a very basic and simple plugin that simply collects information about the funds of your node, both onchain and offchain funds.

## Funds Collector
## File: ./fundsinfo/fundsinfo.py

## How to use this plugin in Core Lightning ?
1. In your terminal run:
```
lightning-cli rpc_method [options]
```
or
```
lightning-cli -k rpc_method [option=value]
```

### Possible options and description
1. all - shows all types of funds separatly, onchain, offchain and misc
2. onchain - shows only the onchain wallet funds
3. offchain - shows only funds in open channels (local balance)
4. misc - shows onchain + offchain funds\n
Note: default=all
