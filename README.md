# Core Lightning plugins collection

## List of plugins
1. fundscollector 


## How to load plugins in Core Lightning ?
1. In your terminal, run:
```
lightning-cli -k plugin start /absolute/path/to/plugin/python/file.py
```
or
```
lightning-cli -k plugin subcommand=start plugin=/absolute/path/to/plugin/python/file.py
```
